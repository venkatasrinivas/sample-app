import { Component, OnInit, Inject } from '@angular/core';
import { Http} from '@angular/http'
@Component({
  selector: 'app-subsubcat',
  templateUrl: './subsubcat.component.html',
  styleUrls: ['./subsubcat.component.css']
})
export class SubsubcatComponent implements OnInit {
  data3;
  data;
  cdata;
  t1;t2;t3;
  get_data;
  constructor( @Inject(Http) public obj) { }

  cat(){
    this.obj.get("subsubcatfile/met").subscribe(
      res=>{
        this.data3=JSON.parse(res._body)
      }
    )
  } //cat close  drop downe
 catget(){    
    this.obj.get("catfile/getdata").subscribe(
      res=>{
        this.data=JSON.parse(res._body)
        // alert(this.data)
         console.log(this.data,"hello")
      })
        } //sub_cat close   drop downe
  

  fun2(){
    var val={catid:this.t1}
    
    this.obj.post("catfile/subcat",val).subscribe(res=>{
      
       this.cdata=JSON.parse(res._body)
      console.log(this.obj)
      alert(this.obj)
    })
    
  }
  
  subsubcat_ins(){
    var ob={catid:this.t1,scatid:this.t2,ssubcat:this.t3}
    alert(ob)
    this.obj.post("catfile/sscsave",ob).subscribe(
    res=>{
      alert(res._body)
      this.subsubcatget();
    })
  }

  //--------getting sub sub cat table values
  subsubcatget(){
    this.obj.get("catfile/sscatget").subscribe(
    res=>{
      this.get_data=JSON.parse(res._body)
      
    })
  }

  //----------update------------------
  temp=0;catId;scatId;ssubCat;oldobj;
  update(ob){
    this.temp=ob._id;
    this.catId=ob.catId;
    this.scatId=ob.scatId;
    this.ssubCat=ob.ssubCat;
    this.t1=ob.catId;
    //this.sub_cat();
    this.t2=ob.scatId;
    this.oldobj={catId:this.catId,scatId:this.scatId,ssubCat:this.ssubCat}
  }
  //---------save-----------------------
  newobj;
  save(){
    this.catId=this.t1;
    this.scatId=this.t2;
    this.newobj={catId:this.catId,scatId:this.scatId,ssubCat:this.ssubCat}
    var ob=[this.oldobj,this.newobj]
    console.log(ob)
    this.obj.post("subsubcatfile/update",ob).subscribe(
      res=>{
        alert(res._body);
      }
    )
    this.temp=0;
    this.subsubcatget();
  }





  ngOnInit() {
   this.catget()
   this.subsubcatget()
   
  }

}
