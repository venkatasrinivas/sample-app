import { Component, OnInit,Inject } from '@angular/core';
import {Http} from '@angular/http'
import { resolveDep } from '@angular/core/src/view/provider';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.css']
})

export class CatComponent implements OnInit {
  t1;data;

  constructor(@Inject (Http)  public  obj) { }
   
  fun1(){
    var data={uname:this.t1}
    alert(data.uname)
    this.obj.post("catfile/save",data).subscribe(function(dt){
      alert(dt._body)
      
    })
    this.t1="";
    this.fun2();
  }
  
  fun2(){    
    this.obj.get("catfile/getdata").subscribe(
      res=>{
        this.data=JSON.parse(res._body)
        //alert(this.data)
      })
        }
  
  ngOnInit() {
    this.fun2();
  }

  //----update-----
  temp=0;text1;oldobj;newobj;
  update(ob){
    this.temp=ob._id
    this.text1=ob.uname;
    this.oldobj={uname:this.text1};
   // alert(this.oldobj.uname)
  }

  updatesave(){
   this.newobj={uname:this.text1}
   //alert(this.newobj.uname)
   var data =[this.oldobj,this.newobj]
   this.obj.post("/catfile/updatesave",data).subscribe(
     res=>{
       alert(res._body)
       this.temp=0;
       this.fun2();
   
     })
  }

}
