import { Component, OnInit, Inject} from '@angular/core';
import {Http} from '@angular/http'
import { Subscriber } from 'rxjs';
@Component({
  selector: 'app-subcat',
  templateUrl: './subcat.component.html',
  styleUrls: ['./subcat.component.css']
})
export class SubcatComponent implements OnInit {
data;subcat;catid;data1;temp=0;text1;
  constructor(@Inject(Http) public obj) { }
  
  
  fun2(){    
    this.obj.get("catfile/getdata").subscribe(
      res=>{
        this.data=JSON.parse(res._body)
        //alert(this.data)
      })
        }
  
    subcatins(){
      var ob={subcat:this.subcat,catid:this.catid}
      alert(ob)
      this.obj.post("/savedata/subcatins",ob).subscribe(
        
        dt=>{
          
        })    
        this.subcatget();
        this.save();
    }

    subcatget(){
      
      this.obj.get("/savedata/subcatget").subscribe(
        res=>{
          console.log(res.body,"helllooooo")
          this.data1=JSON.parse(res._body)
        })
    }

    oldobj;
    subcatup(ob){
       this.temp=ob._id;
       this.text1=ob.subcat;  
       this.oldobj={subcat:this.text1,catid:ob.catid}           
    }
    newobj;
  save(){
    //alert(this.catid)
    this.newobj={subcat:this.text1,catid:this.catid}
    var arr=[this.oldobj,this.newobj]
    this.obj.post("savedata/save",arr).subscribe(res=>{
      alert(res._body)
      this.subcatget();
    })
    
    this.temp=0;   
    
  }
  active(act){
    act.active=1;
    var ob={_id:act._id,active:act.active}
    this.obj.post("savedata/active",ob).subscribe(
      res=>{
        alert(res._body)
      }
    )
    
  }

inactive(act1){
    act1.active=0;
    var ob={_id:act1._id,active:act1.active}
    this.obj.post("savedata/inactive",ob).subscribe(
      res=>{
        alert(res._body)
      }
    )
    
  }
  ngOnInit() {
    this.fun2();
    this.subcatget();

  }

}
