import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import {HttpModule} from "@angular/http"
import {FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { CatComponent } from './cat/cat.component';
import { SubcatComponent } from './subcat/subcat.component';
import { SubsubcatComponent } from './subsubcat/subsubcat.component';
import { ProductComponent } from './product/product.component';
import { BrandComponent } from './brand/brand.component';



var obj =[{
  path:"cat",component:CatComponent
},{
  path:"subcat",component:SubcatComponent
},{
  path:"subsubcat",component:SubsubcatComponent
},{path:"product",component:ProductComponent
},{path:"brand",component:BrandComponent}]

var rout=RouterModule.forRoot(obj)

@NgModule({
  declarations: [
    AppComponent,
    CatComponent,
    SubcatComponent,
    SubsubcatComponent,
    ProductComponent,
    BrandComponent,
    
  ],
  imports: [
    BrowserModule,rout,HttpModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
