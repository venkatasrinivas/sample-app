 exp=require("express")
 bp=require("body-parser")
 var expfile=require("express-fileupload")
 catfile = require("./myfiles/catfile")
 subcat = require("./myfiles/subcatfile")
 sub =require("./myfiles/subsubcat")
 product = require("./myfiles/product")
app=exp()
app.use(bp.json())
app.use(expfile())

app.use("/catfile",catfile)
app.use("/savedata",subcat)
app.use("/subsubcat",sub)
app.use("/product",product)
app.listen(3000)
console.log("server started")

app.post("/upload",function(req,res){
    
   //console.log(req.files.file1.name)
   iname= req.files.file1
   iref= req.files.file1
    console.log(iname,"image name")
   d= new Date()
   d=d/1000;
   iname="img"+parseInt(d)+"_"+iname.name;
   console.log(iname,"iname value")
  iref.mv("images/"+iname)
  res.redirect("http://localhost:4200/subsubcat")
})

